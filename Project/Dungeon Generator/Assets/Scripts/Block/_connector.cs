﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _connector : MonoBehaviour
{
    [Header("References")]
    public _block parent;
    public _block ConnectedTo;

    [Header("Parameters")]
    public string rawName;
    public string baseName;
    // Start is called before the first frame update
    void Start()
    {
        //ignore collisions for blocks and connectors
        Physics2D.IgnoreLayerCollision(8, 8);
        Physics2D.IgnoreLayerCollision(9, 9);
        //set the parent object
        parent = transform.parent.GetComponent<_block>();
        //set the connectors name
        rawName = name;
        baseName = name + ":" + parent.name;        
        name = baseName;
    }

    // Update is called once per frame
    void Update()
    {
        //set the connectors name
        baseName = rawName + ":" + parent.name;
        if (ConnectedTo!=null)
        {
            name = baseName + " [ Connected to : " + ConnectedTo.name + " ]";
        }
        else
        {
            name = baseName;
        }
    }
}
