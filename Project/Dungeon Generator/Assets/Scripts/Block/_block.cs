﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _block : MonoBehaviour
{
    [Header("References")]
    public _connector[] connectors;
    public _block[] connectedTo;

    [Header("Parameters")]
    public static int index;
    public int id = 0;
    public int nonNullConnecions = 0;
    // Start is called before the first frame update
    void Start()
    {
        //set the blocks name
        id = index;
        index++;
        if (name == "Block")
        {
            name = "block : [ " + id + " ]";
        }
        else
        {
            name = name + " : [ " + id + " ]";
        }
    }

    // Update is called once per frame
    void Update()
    {
        checkConnectors();
        //for debugging only
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        switch(nonNullConnecions)
        {
            case 0:
                sr.color = Color.white;
                break;
            case 1:
                sr.color = new Color(0.819226f, 1, 0);
                break;
            case 2:
                sr.color = new Color(0.2705615f, 1, 0);
                break;
            case 3:
                sr.color = new Color(0, 1, 0.6138928f);
                break;
            case 4:
                sr.color = new Color(0, 0.7627776f, 1);
                break;
            default:
                sr.color = Color.white;
                break;

        }
    }

    //remove this block when its destroyed
    private void OnDestroy()
    {
        try
        {
            transform.parent.GetComponentInParent<room>().blocks.Remove(this.gameObject);
        }
        catch
        {
            //exception thrown due to a null reference
        }
    }
    //update connected block references
    void checkConnectors()
    {
        //reset the connected blocks
        connectedTo = new _block[connectors.Length];
        nonNullConnecions = 0;
        //check each connector for a connection
        for(int i=0;i<connectors.Length;i++)
        {
            connectedTo[i] = connectors[i].ConnectedTo;
            if(connectedTo[i]!=null)
            {
                nonNullConnecions++;
            }
        }
        

    }

    //for detecting which blocks this block is connected to
    public void OnTriggerStay2D(Collider2D other)
    { 
        if (other.tag == "connector")
        {
            other.GetComponent<_connector>().ConnectedTo = this;
            other.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "connector")
        {
            if (this == other.GetComponent<_connector>().ConnectedTo)
            {
                other.GetComponent<_connector>().ConnectedTo = null;
                other.GetComponent<SpriteRenderer>().enabled = true;
            }
        }
    }
}
