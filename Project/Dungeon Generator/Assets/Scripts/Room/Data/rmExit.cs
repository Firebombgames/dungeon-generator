﻿using System.Collections;
using System.Collections.Generic;


//holds data on the exit
public class rmExit
{
    public rmVec2 position;
    public int size = 0;
    public direction exitDir;
}
