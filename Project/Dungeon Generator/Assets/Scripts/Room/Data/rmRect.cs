﻿using System.Collections;
using System.Collections.Generic;

//for holding the rooms bounds
public class rmRect
{
    rmVec2 position;
    rmVec2 size;

    public rmRect(int X, int Y, int width, int height)
    {
        position = new rmVec2(X, Y);
        size = new rmVec2(width, height);
    }
}