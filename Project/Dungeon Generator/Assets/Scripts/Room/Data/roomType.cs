﻿using System.Collections;
using System.Collections.Generic;

public enum roomType
{
	Treasure,
	Upgrade,
	Combat,
	Boss,
	Empty
}
