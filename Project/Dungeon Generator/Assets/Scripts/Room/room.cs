﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class room : MonoBehaviour
{
    [Header ("References")]
    public roomNode nodeData;

    [Header ("Child References")]
    public List<GameObject> blocks = new List<GameObject>();
    public List<GameObject> spawners = new List<GameObject>();
    public GameObject centerCollider;
    public GameObject topWall;
    public GameObject bottomWall;
    public GameObject rightWall;
    public GameObject leftWall;
    public GameObject corners;
    public List<rmExit> exits = new List<rmExit>();
    public GameObject block;

    [Header("Data")]
    public Vector2 position;
    public string _name;
    public static int S_id = 0;
    public int id;
    public Rect bounds;
    public roomType type;

    [Header("ChildData")]
    public List<blockQueue> blockSpawnQueue = new List<blockQueue>();

    //start method
    public void Start()
    {
        initialize();
        spawnChildObjects();
        generateCorners();
        spawnWalls();
    }

    //here we can do our initial setup via spawning all of the objects
    public void spawnChildObjects()
    {
        //set up the center collider
        centerCollider = spawnNewObject(transform, Vector2.zero);
        centerCollider.name = name + " : Center Collider";
        centerCollider.AddComponent<BoxCollider2D>();
        centerCollider.GetComponent<BoxCollider2D>().isTrigger = true;
        
        //set up the corners
        corners = spawnNewObject(transform, Vector2.zero);
        corners.name = name + " : Corners";
       
        //set up the walls
        topWall = spawnNewObject(transform, Vector2.zero);
        topWall.name = name + " : Top Wall";
        topWall.AddComponent<BoxCollider2D>();
        topWall.GetComponent<BoxCollider2D>().enabled = false;

        bottomWall = spawnNewObject(transform, Vector2.zero);
        bottomWall.name = name + " : Bottom Wall";
        bottomWall.AddComponent<BoxCollider2D>();
        bottomWall.GetComponent<BoxCollider2D>().enabled = false;

        leftWall = spawnNewObject(transform, Vector2.zero);
        leftWall.name = name + " : Left Wall";
        leftWall.AddComponent<BoxCollider2D>();
        leftWall.GetComponent<BoxCollider2D>().enabled = false;

        rightWall = spawnNewObject(transform, Vector2.zero);
        rightWall.name = name + " : Right Wall";
        rightWall.AddComponent<BoxCollider2D>();
        rightWall.GetComponent<BoxCollider2D>().enabled = false;


    }

    //spawn a new gameObject
    public GameObject spawnNewObject(Transform parent, Vector2 position)
    {
        GameObject g = new GameObject();
        g.transform.position = position;
        g.transform.parent = parent;
        return g;
    }

    /// <summary>
    /// TO-DO setup processing of node data from dungeon generator
    /// </summary>
    public void processNodeData()
    {

    }
    //initialize some data
    public void initialize()
    {
        if (bounds.width % 2 != 0)
        {
            //make the size one smaller
            bounds.width--;
        }
        if (bounds.height % 2 != 0)
        {
            //make the size one smaller
            bounds.height--;
        }
    }
    //create a wall
    public void generateWall(wallMode mode)
    {
        Transform parent;
        float x = 0;
        float y = 0;
        //get a more accessable verson of the size
        Vector2 roomSize = new Vector2(bounds.width, bounds.height);
        int goal = 0;
        //set start point
        switch (mode)
        {
            case wallMode.top:
                x = -roomSize.x / 2 + 1;
                y = roomSize.y / 2;
                mode = wallMode.horizontal;
                goal = (int)roomSize.x - 1;
                parent = topWall.transform;
                break;
            case wallMode.bottom:
                x = -roomSize.x / 2 + 1;
                y = -roomSize.y / 2;
                mode = wallMode.horizontal;
                goal = (int)roomSize.x - 1;
                parent = bottomWall.transform;
                break;
            case wallMode.right:
                x = roomSize.x / 2;
                y = roomSize.y / 2 - 1;
                mode = wallMode.vertical;
                goal = (int)roomSize.y - 1;
                parent = rightWall.transform;
                break;
            case wallMode.left:
                x = -roomSize.x / 2;
                y = roomSize.y / 2 - 1;
                mode = wallMode.vertical;
                goal = (int)roomSize.y - 1;
                parent = leftWall.transform;
                break;
            default:
                throw new System.Exception("Error 0001 [" + name + "]: Attempted to spawn wall with invalid wallMode \n " +
                    "you probably used horizontal or vertical when another value was expected");
        }
        
        //start generating the wall
        for(int i=1; i<=goal ;i++)
        {
            spawnBlock(parent.name + " block : ( " + name + " ) ", new Vector2(x, y), parent);
            switch(mode)
            {
                case wallMode.horizontal:
                    x++;
                    break;
                case wallMode.vertical:
                    y--;
                    break;
            }
        }
    }

    //spawn the walls
    public void spawnWalls()
    {
        generateWall(wallMode.top);
        generateWall(wallMode.right);
        generateWall(wallMode.left);
        generateWall(wallMode.bottom);
    }

    //create the corners
    public void generateCorners()
    {
        //get a more accessable verson of the size
        Vector2 roomSize = new Vector2(bounds.width, bounds.height);
        //now spawn the actual corners

        //top left
        spawnBlock("Top Left : ( " + name + " ) ",  new Vector2(-(roomSize.x / 2), (roomSize.y/2)), corners.transform);
        //top right
        spawnBlock("Top Right : ( " + name + " ) ", new Vector2((roomSize.x / 2), (roomSize.y / 2)), corners.transform);
        //bottom left
        spawnBlock("Bottom Left : ( " + name + " ) ", new Vector2(-(roomSize.x / 2), -(roomSize.y / 2)), corners.transform);
        //bottom right
        spawnBlock("Bottom Right : ( " + name + " ) ", new Vector2((roomSize.x / 2), -(roomSize.y / 2)), corners.transform);
    }

    //spawn a single block
    public void spawnBlock(string name,Vector2 offset, Transform parent)
    {
        GameObject g=Instantiate(block, offset, Quaternion.identity, parent);
        g.name = name;
        blocks.Add(g);
    }

}

public enum wallMode
{
    vertical, 
    horizontal,
    top,
    bottom,
    right,
    left
}
